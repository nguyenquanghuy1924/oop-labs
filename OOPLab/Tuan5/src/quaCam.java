/**
 * quaCam bieu dien qua cam
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2/10/2018
 */
public class quaCam extends Hoaqua{
    //thuoc tinh class
    private String huongvi;
    //ham khoi tao
    public quaCam(){
        this.huongvi = "chua";
    }
    //setter
    public void setHuongvi(String huongvi) {
        this.huongvi = huongvi;
    }
    //getter
    public String getHuongvi() {
        return huongvi;
    }
}
