/**
 * Rectangle bieu dien hinh chu nhat
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2/10/2018
 */

public class Rectangle extends Shape {
    //thuoc tinh class
    private double  width = 1.0;
    private double length = 1.0;
    //ham khoi tao khong tham so
    public Rectangle(){}
    //ham khoi tao co tham so
    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }
    //ham khoi tao co tham so
    public Rectangle(double width, double length, String color, boolean filled){
        this.width = width;
        this.length = length;
        this.setColor(color);
        this.setFilled(filled);
    }

    //setter
    public void setWidth(double width) {
        this.width = width;
    }
    //getter
    public double getWidth() {
        return width;
    }
    //setter
    public void setLength(double length) {
        this.length = length;
    }
    //getter
    public double getLength() {
        return length;
    }

    /**
     * tinh dien tich
     * @return dien tich
     */
    public double getArea(){
        return this.length*this.width;
    }

    /**
     * tinh chu vo
     * @return chu vi
     */
    public double getPerimeter(){
        return 2*(this.length+this.width);
    }

    @Override
    /**
     * tra lai thuoc tinh cua
     */
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", length=" + length +
                '}';
    }
}

