/**
 * Circle bieu dien hin tron
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2/10/2018
 */
public class Circle extends Shape {
    //thuoc tinh class
    private double radius = 1.0;
    private  final double PI = Math.PI;
    //ham khoi tao khong tham so
    public Circle(){}
    //ham khoi tao co tham so
    public Circle(double radius){
        this.radius = radius;
    }
    //ham khoi tao co tham so
    public Circle(double radius, String color, boolean filled){
        this.radius = radius;
        this.setColor(color);
        this.setFilled(filled);
    }
    //setter
    public void setRadius(double radius) {
        this.radius = radius;
    }
    //getter
    public double getRadius() {
        return radius;
    }

    /**
     * tinh dien tich hinh tron
     * @return dien tich hinh tron
     */
    public double getArea(){
        return radius*2*PI;
    }

    /**
     * tinh chu vi hinh tron
     * @return chu vi hinh tron
     */
    public double Perimeter(){
        return PI*2*radius;
    }

    @Override
    /**
     * tra lai gia tri cua thuoc tinh ham
     */
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", PI=" + PI +
                '}';
    }
}
