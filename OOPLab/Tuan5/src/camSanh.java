/**
 * camSanh bieu dien cam sanh
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2/10/2018
 */
public class camSanh extends Hoaqua{
    //khai bao thuco tinh class
    private String nguongoc;
    private String giaban;
    private String ngaynhap;

    /**
     * ham khoi tao
     */
    public camSanh(){
        this.nguongoc = "Bac Giang";
        this.giaban = "23000 dong";
        this.ngaynhap = "22/9/2018";
    }

    //setter
    public void setNguongoc(String nguongoc) {
        this.nguongoc = nguongoc;
    }

    //getter
    public String getNguongoc() {
        return nguongoc;
    }

    //setter
    public void setGiaban(String giaban) {
        this.giaban = giaban;
    }
    //getter
    public String getGiaban() {
        return giaban;
    }
    //setter
    public void setNgaynhap(String ngaynhap) {
        this.ngaynhap = ngaynhap;
    }
    //getter
    public String getNgaynhap() {
        return ngaynhap;
    }
}

