/**
 * Hoa qua bieu dien thuoc tinh hoa qua
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2/10/2018
 */
public class Hoaqua {
    // khai bao thuoc tinh
    private String hinhdang;
    private String mausac;

    // ham khoi tao
    public Hoaqua(){
        this.hinhdang = "tron";
        this.mausac = "cam";
    }

    //setter
    public void setHinhdang(String hinhdang) {
        this.hinhdang = hinhdang;
    }
    //getter
    public String getHinhdang() {
        return hinhdang;
    }

    //setter
    public void setMausac(String mausac) {
        this.mausac = mausac;
    }
    //getter
    public String getMausac() {
        return mausac;
    }
}
