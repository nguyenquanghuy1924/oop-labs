/**
 * Square bieu dien hin vuong
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2/10/2018
 */
public class Square extends Rectangle {
    // thuoc tinh class
    private  double side = 1.0;
    //ham khoi tao khong tham so
    public Square(){}
    //ham khoi tao co tham so
    public Square(double side){
        this.side = side;
    }
    //ham khoi tao co tham so
    public Square(double side, String color, boolean filled){
        this.side = side;
        this.setColor(color);
        this.setFilled(filled);
    }
    //setter
    public void setSide(double side){
        this.side = side;
    }
    //getter
    public double getSide() {
        return side;
    }
    //setter
    public void setWidth(double side){
        super.setWidth(side);
        super.setLength(side);
    }
    //setter
    public void setLength(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    /**
     * tra lai ket qua cua class
     */
    public String toString() {
        return "Square{" +
                "side=" + side +
                '}';
    }
}
