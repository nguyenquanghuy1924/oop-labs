/**
 * Shape bieu dien hinh dang hinh hoc
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2/10/2018
 */

public class Shape {
    //khai bao thuoc tinh
    private String color = "red";
    private boolean filled = true;
    // ham khoi tao khong tham so
    public Shape(){}
    //ham khoi tao co tham so
    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }
    //setter
    public void setColor(String color) {
        this.color = color;
    }
    //getter
    public String getColor() {
        return color;
    }
    //setter
    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    //getter
    public boolean isFilled() {
        return filled;
    }

    @Override
    /**
     * tra lai gia tri thuoc tinh cua class
     */
    public String toString() {
        return "Shape{" +
                "color='" + color + '\'' +
                ", filled=" + filled +
                '}';
    }
}
