import org.junit.Test;
import static org.junit.Assert.*;

public class TestPrintBMI {
    Excersise e = new Excersise();
    @Test
    public void testBMI1(){
        System.out.println("Test BMI:" );
        assertEquals("Bình Thường", e.printBMI(1.76, 70));
    }
    @Test
    public void testBMI2(){
        System.out.println("Test BMI:" );
        assertEquals("Béo Phì", e.printBMI(1.65, 80));
    }
    @Test
    public void testBMI3(){
        System.out.println("Test BMI:" );
        assertEquals("Thiếu Cân", e.printBMI(1.6, 46));
    }
    @Test
    public void testBMI4(){
        System.out.println("Test BMI:" );
        assertEquals("Thừa Cân", e.printBMI(1.8, 75));
    }
    @Test
    public void testBMI5(){
        System.out.println("Test BMI:" );
        assertEquals("Bình Thường", e.printBMI(1.65, 60));
    }

}
