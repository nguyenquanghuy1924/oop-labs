import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class TestFindMinArray {
    Excersise e = new Excersise();
    @Test
    public void testfindMinArray1(){
        System.out.println("Test min array: ");
        int[] a = {1, 2, 4, 5, 6};
        assertEquals("min is 1", 1, e.findMinArray(a));

    }
    @Test
    public void testfindMinArray2(){
        System.out.println("Test min array: ");
        int[] b = {-1, 2, 4, 5, -6, 0, 6, 7 , 100, -1000};
        assertEquals("min is -1000", -1000, e.findMinArray(b));
    }
    @Test
    public void testfindMinArray3(){
        System.out.println("Test min array: ");
        int[] c = {1, -7, 4, 5, 9, 100};
        assertEquals("min is -7", -7, e.findMinArray(c));
    }
    @Test
    public void testfindMinArray4(){
        System.out.println("Test min array: ");
        int[] d = {1, 1, 1, 1, 1, 1 , 1, 1, 1};
        assertEquals("min is 1", 1, e.findMinArray(d));
    }
    @Test
    public void testfindMinArray5(){
        System.out.println("Test min array: ");
        int[] h = {-11, -9, -2, -10, -9};
        assertEquals("min is -11", -11, e.findMinArray(h));
    }

}
