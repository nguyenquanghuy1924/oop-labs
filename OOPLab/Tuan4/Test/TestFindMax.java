import org.junit.Test;
import static org.junit.Assert.*;

public class TestFindMax {
    Excersise e = new Excersise();

    @Test
    public void testfindMax1(){
        System.out.println("Text Max:");
        assertEquals("16 is larger than 4",16,  e.findMax(16, 4));
    }
    @Test
    public void testfindMax2(){
        System.out.println("Text Max:");
        assertEquals("20 is larger than 4",20,  e.findMax(4, 20));
    }
    @Test
    public void testfindMax3(){
        System.out.println("Text Max:");
        assertEquals("25 is larger than -10",25,  e.findMax(25, -10));

    }
    @Test
    public void testfindMax4(){
        System.out.println("Text Max:");
        assertEquals("-4 is larger than -10",-4,  e.findMax(-10, -4));
    }
    @Test
    public void testfindMax5(){
        System.out.println("Text Max:");
        assertEquals("1 is larger than 0",1,  e.findMax(0, 1));
    }


}
