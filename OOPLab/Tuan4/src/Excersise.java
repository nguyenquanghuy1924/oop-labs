/**
 * Excersise la ung de tim max 2 số nguyên, tìm min của 1 dãy, và in ra chỉ số BMI
 * @author Nguyen Quang Huy
 * @version 1.0
 * since 2018-09-25
 */

public class Excersise {
    /**
     * findMax la hamm de
     * @param a so nguyen thu nhat
     * @param b so nguyen thu hai
     * @return max cua 2 so nguyen
     */
    public static int findMax(int a, int b){
        return Math.max(a, b);
    }

    /**
     * findMinArray la ham de tim min cua day
     * @param a la day so a
     * @return min cua day
     */
    public static int findMinArray(int a[]){
        int minn = a[0];
        for(int i = 0; i < a.length;  i++){
            if(minn > a[i]){
                minn = a[i];
            }
        }
        return minn;
    }

    /**
     * printBMI la ham de thong bao ket qua theo chi so BMi
     * @param h la chieu cao
     * @param w la can nang
     * @return ket qua thong bao theo chi so BMI
     */
    public static String printBMI(double h, double w){
        double bmi = w/(h*h);
        if(bmi < 18.5){
            return "Thiếu Cân";
        }
        else if (bmi >= 18.5 && bmi < 23 ){
            return "Bình Thường";
        }
        else  if(bmi >= 23 && bmi < 24){
            return "Thừa Cân";
        }
        else{
            return  "Béo Phì";
        }
    }
}

