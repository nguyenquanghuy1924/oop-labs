/**
 * StudentManagement la class de bieu dien thong tin sv
 * @param name la ten cua sv
 * @param id la mssv
 * @param group la nhom cua sv
 * @param email la mail cua sv
 */
class Student {
    private String name;
    private String id;
    private String group;
    private String email;
    public Student(){
        this.name = "Student";
        this.id = "000";
        this.group = "K59CB";
        this.email = "uet@vnu.edu.vn";
    }
    public Student(String n, String sid, String em){
        this.name = n;
        this.id = sid;
        this.group = "K59CB";
        this.email = em;
    }
    public Student(Student s){
        this.name = s.name;
        this.id = s.id;
        this.group = s.group;
        this.email = s.email;
    }

    /**
     * ham getName  lay ten sv
     * @return ten sinh vien
     */
    public String getName() {
        return this.name;
    }

    /**
     * ham setName  dat ten sv
     * @param n ten can nhap
     */
    public void setName(String n){
        this.name = n;
    }
    /**
     * ham getId  lay id  cua sv
     * @return tra ve id cua sv
     */
    public String getId(){
        return this.id;
    }
    /**
     * ham setId  dat ten id cua sv
     * @param n id can nhap
     */
    public void setId(String n){
        this.id = n;
    }
    /**
     * getGroup de lay ten cua group
     * @return tra ve group cua sv
     */
    public String getGroup(){
        return this.group;
    }
    /**
     * ham setGroup de dat ten group cua sv
     * @param group group can nhap
     */
    public void setGroup(String n){
        this.group = n;
    }
    /**
     * ham getEmail lay ten email cua sv
     * @return  tra ve dia chi email cua sv
     */
    public String getEmail(){
        return this.email;
    }
    /**
     * ham setEmail de dat ten email
     * @param n email can nhap
     */
    public void setEmail(String n){
        this.email = n;
    }

    /**
     * ham getInfo lay thong tin cua sv
     * @param info chua thong tin sv
     * @return tra ve thong tin cua sv
     */
    String getInfo(){
        String info = name + " " + id + " " + group + " " + email;
        return info;
    }
}
public class Tuan3 {
    Student[] sv = new Student[100];

    /**
     * Ham SameGroup kiem tra su giong nhau ve group cua sv
     * @param s1 thong tin sv 1
     * @param s2 thong tin sv 2
     * @return True neu cung group false neu khac group
     */
    public static boolean sameGroup(Student sv1, Student sv2){
        if(sv1.getGroup() != sv2.getGroup()){
            return false;
        }
        else return true;

    }

    /**
     * ham studentByGroup  in ra thong tin nhung sv co cung ten group
     */
    public void studentByGroup(){

        String[] group_uniform = new String[100];
        boolean[] kt = new boolean[100];
        int n = 0;

        /**
         * vong for de loc ten group la duy nhat khong co bi trung lap
         */
        for(int i = 0; i < sv.length; i ++){
            kt[i] = true;
           for(int j = 0; j < i; j ++) {
               if (sv[i].getGroup() == sv[j].getGroup()) {
                   kt[i] = false;
                   break;
               }
           }
        }
        /**
         * vong for de gan ten group la doc nhat vao mang group_uniform
         */
        for(int i = 0; i < sv.length; i ++){
            if(kt[i] == true){
                group_uniform[n] = sv[i].getGroup();
                n++;
            }
        }
        /**
         * vong for de in ra ten hoc sinh theo lop
         */
        for(int i = 0; i < n; i++){
            System.out.print(group_uniform[i] + ": ");
            for(int j = 0; j < sv.length; j++){
                if(sv[j].getGroup() == group_uniform[i] ){
                    System.out.print(sv[j].getName() + ", ");
                }
            }
            System.out.println();
        }
    }

    /**
     * ham removeStudent xoa sv co id can xoa
     * @param id la id cua sv can xoa
     * @return tra ve true neu sv duoc kiem tra co id giong id xet
     */
    public  boolean removeStudent(String id){
        boolean kt = true;
        for(int i = 0; i < sv.length; i ++) {
            if (sv[i].getId() == id) {
                kt = false;
            }
        }
        return kt;
    }
    public void init(){
        for(int i = 0; i < sv.length; i ++){
            sv[i] = new Student();
        }
    }

    public static void main(String[] args) {
        Tuan2 huy = new Tuan2();
        huy.init();
        Student sv1 = new Student();
        Student sv2 = new Student();
        Student sv3 = new Student();
        sv1.setName("Nguyen Quang Huy");
        sv1.setId("17021263");
        sv1.setEmail("17021263@uetmail.edu.vnu");
        sv1.setGroup("K62CLC");
        sv2.setGroup("K63CLC");
        sv2.setName("hello");
        sv3.setGroup("K63CLC");
        sv3.setName("hihi");
        huy.sv[0] = new Student(sv1);
        huy.sv[1] = new Student(sv2);
        huy.sv[2] = new Student(sv3);
        huy.studentByGroup();


        //System.out.println(sv1.getName());
        //System.out.println(sv1.getInfo());
        //System.out.println(sv2.getInfo());
        //System.out.println(sv3.getInfo());
        //System.out.println(sameGroup(sv2, sv3));
        //System.out.println(sameGroup(sv1, sv2));
    }
}
