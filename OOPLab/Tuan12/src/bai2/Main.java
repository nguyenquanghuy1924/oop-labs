/**
 * @author Nguyen Quang Huy
 * verion 1.0
 * since 20-11-2018
 * */

package bai2;

public class Main{
    public static void main(String[] args) {
        int[] a = {1, 10, 5, -1, 4, 9};
        Context context = new Context(new insertionsort());
        context.executeStrategy(a);
        for(int i = 0; i < a.length; i ++){
            System.out.println(a[i]);
        }
    }
}
