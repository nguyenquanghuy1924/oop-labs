/**
 * @author Nguyen Quang Huy
 * verion 1.0
 * since 20-11-2018
 * */

package bai2;

public class insertionsort implements Strategy {
    @Override
    public void doSort(int[] a) {
        for (int j = 1; j < a.length; j++) {
            int key = a[j];
            int i = j-1;
            while ( (i > -1) && (  a[i] > key ) ) {
                a[i+1] = a[i];
                i--;
            }
            a[i+1] = key;
        }
    }
}
