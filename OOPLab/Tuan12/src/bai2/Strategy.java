/**
 * @author Nguyen Quang Huy
 * verion 1.0
 * since 20-11-2018
 * */

package bai2;

public interface Strategy {
    public void doSort(int[] a);
}

