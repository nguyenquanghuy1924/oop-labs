/**
 * @author Nguyen Quang Huy
 * verion 1.0
 * since 20-11-2018
 * */

package bai2;

public class bubblesort implements Strategy {
    @Override
    public void doSort(int[] a) {
        for(int i = 0; i < a.length; i ++){
            for(int j = i+1; j < a.length; j++){
                if(a[j] < a[i]){
                    int t = a[i];
                    a[i] = a[j];
                    a[j] = t;
                }
            }
        }
    }
}
