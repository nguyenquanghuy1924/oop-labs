/**
 * @author Nguyen Quang Huy
 * verion 1.0
 * since 20-11-2018
 * */
package bai2;

public class Context {
    private Strategy strategy;

    public Context(Strategy strategy){
        this.strategy = strategy;
    }
    public void executeStrategy(int[] a){
        strategy.doSort(a);
    }
}
