/**
 * class ClassCastException thuc hien loi ClassCastException
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018-10-09
 */

package OOP.EX2;

public class ClassCastException{
    /**
     * ham intToBoolean kiem tra chuyen int sang kieu boolen
     * @param n kieu int
     * @return true or false
     * @throws java.lang.ClassCastException
     */
    public static boolean intToBoolean(int n) throws java.lang.ClassCastException
    {
        if (n<0 || n>1) throw new java.lang.ClassCastException("Ep kieu khong hop le");
        if(n == 1) return true;
        else return false;
    }

    /**
     * Test thu
     * @param args
     */
    public static void main(String[] args) {
        if(intToBoolean(1)) System.out.println("True");
        else System.out.println("False");
        intToBoolean(10);
    }

}
