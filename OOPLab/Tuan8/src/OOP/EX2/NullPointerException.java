/**
 * * class ArrayIndexOfBoundsException thuc hien loi ArrayIndexOfBoundsException
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018-10-09
 */

package OOP.EX2;

import java.util.ArrayList;
import java.io.*;
import java.util.*;

public class NullPointerException {
    /**
     * ham printString bat loi Nullpointer
     * @param s string
     * @throws java.lang.NullPointerException
     */
    public static void printString(String s) throws java.lang.NullPointerException {
        if(s == null) throw new java.lang.NullPointerException("Xau rong");
        System.out.println(s);
    }


    /**
     * ham main test thu
     * @param args khong su dung
     * @throws java.lang.Exception
     */
    public static void main(String[] args) {
         //Test NullpointerException
        printString("A");
        printString(null);

    }
}
