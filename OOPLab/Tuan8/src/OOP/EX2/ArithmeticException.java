/**
 * class  ArithmeticException thuc hien loi  ArithmeticException
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018-10-09
 */

package OOP.EX2;

public class ArithmeticException {
    /**
     * ham divsion bat loi khi chia cho 0
     * @throws java.lang.ArithmeticException
     */
    public static void division() throws java.lang.ArithmeticException
    {
        throw new java.lang.ArithmeticException("Loi chia cho 0");
    }

    /**
     * ham main test thu
     * @param args khong su dung
     */
    public static void main(String[] args) {

        try{
            division();
        }catch (java.lang.ArithmeticException e){

        }

    }
}
