/**
 * class FileNotFoungdException thuc hien loi FileNotFoundException
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018-10-09
 */
package OOP.EX2;

import java.io.*;
import java.util.Scanner;

public class FileNotFoundException {
    /**
     * ham readfile bat loi neu khong ton tai file
     * @throws java.io.FileNotFoundException
     */
    public static void readFile() throws java.io.FileNotFoundException
    {
        throw new java.io.FileNotFoundException();
    }

    /**
     * ham main test thu
     * @param args khong su dung
     */
    public static void main(String[] args) {
        try {
            readFile();
        }catch (java.io.FileNotFoundException e) {

        }

    }

}
