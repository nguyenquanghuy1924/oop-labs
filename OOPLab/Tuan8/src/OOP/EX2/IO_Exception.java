package OOP.EX2;

import java.io.*;

public class IO_Exception {

    public static void check() throws IOException {
        throw new IOException();
    }

    public static void main(String[] args) throws IOException {
        check();
    }
}
