/**
 * class ArrayIndexOfBoundsException thuc hien loi ArrayIndexOfBoundsException
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018-10-09
 */

package OOP.EX2;

import java.util.ArrayList;

public class ArrayIndexOfBoundsException {
    /**
     * ham printElement bat loi in vi tri khong co trong mang
     * @param a mang a
     * @param i vi tri thu i trong mang
     * @throws java.lang.ArrayIndexOutOfBoundsException
     */
    public static void printElement(ArrayList a, int i) throws java.lang.ArrayIndexOutOfBoundsException
    {
        if(i < 0 || i >= a.size()) throw new java.lang.ArrayIndexOutOfBoundsException("Mang khong co phan tu thu " + i);
        System.out.println(a.get(i));
    }

    /**
     * ham main test thu
     * @param args khong su dung
     */
    public static void main(String[] args) throws java.lang.ArrayIndexOutOfBoundsException {
        //test ArrayIndexOutOfException
        ArrayList a = new ArrayList();
        a.add(10);
        a.add(11);
        printElement(a, 0);
        printElement(a, 2);

    }
}
