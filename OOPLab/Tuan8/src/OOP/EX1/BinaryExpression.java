package OOP.EX1;
/**
 * abstract class BinaryExpression cac phep toan nhi phan
 * @author NguyenQuangHuy
 * @version 1.0
 * @since 2018-10-09
 */

public abstract class BinaryExpression extends Expression {
    private Expression left;
    private Expression right;
    /**
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public BinaryExpression(Expression l, Expression r) {
        left = l;
        right = r;
    }

    protected BinaryExpression() {
    }

    public void setLeft(Expression left) {
        this.left = left;
    }

    public void setRight(Expression right) {
        this.right = right;
    }
    public Expression getLeft() { return left; }
    public Expression getRight() { return right; }



    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public abstract String toString();
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public abstract int evaluate();
}
