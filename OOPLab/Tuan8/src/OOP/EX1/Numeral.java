package OOP.EX1;

public class Numeral extends Expression {
    private int value;
    public Numeral(){
        value = 10;
    }
    public Numeral(int v){
        this.value = v;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public int evaluate() {
        return value;
    }
}
