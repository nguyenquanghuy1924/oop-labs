/**
 * class ExpressionTest test thu
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018-10-09
 */
package OOP.EX1;

public class ExpressionTest {
    /**
     * ham main thuc hien bieu thuc (10^10 - 2 + 2*#)^2
     * @param args khong su dung
     */
    public static void main(String[] args) {
        Numeral n1 = new Numeral(10);
        Numeral n2 = new Numeral(10);
        Square s = new Square(n2);
        Multiplication m1 = new Multiplication(n1, n2);
        System.out.println(m1.toString() + " = " + m1.evaluate());
        Numeral n3 = new Numeral(1);
        Numeral n4 = new Numeral(m1.evaluate());
        Subtraction s1 = new Subtraction(n4, n3);
        System.out.println(s1.toString() + " = " + s1.evaluate() );
        Numeral n5 = new Numeral(2);
        Numeral n6 = new Numeral(3);
        Multiplication m2 = new Multiplication(n5, n6);
        System.out.println(m2.toString() + " = " + m2.evaluate());
        Numeral n7 = new Numeral(s1.evaluate());
        Numeral n8 = new Numeral(m2.evaluate());
        Addition a1 = new Addition(n7, n8);
        System.out.println(a1.toString() + " = " + a1.evaluate());
        Numeral n9 = new Numeral(a1.evaluate());
        Numeral n10 = new Numeral(2);
        Multiplication m3 = new Multiplication(n9, n10);
        System.out.println(m3.toString() + " = " + m3.evaluate());

        try {
            Expression division = new Division(new Numeral(1),new Numeral(0));
            System.out.print(division.toString() + " = ");
            System.out.println(division.evaluate());
        }
        catch (Exception e)
        {
            if (e instanceof ArithmeticException) System.out.println("\nLoi chia cho 0");
        }
    }
}
