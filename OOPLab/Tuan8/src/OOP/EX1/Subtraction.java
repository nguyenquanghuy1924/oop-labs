package OOP.EX1;

public class Subtraction extends BinaryExpression {
    private Expression l;
    private Expression r;
    /**
     * constructor
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public Subtraction(Expression l, Expression r) {
        this.l = l;
        this.r = r;
    }

    /**
     * Ham in bieu thuc
     * @return String
     */
    public String toString() {
        return String.format("%s - %s", l, r);
    }

    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return l.evaluate() - r.evaluate();
    }
}
