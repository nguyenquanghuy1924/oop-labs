package OOP.EX1;

/**
 * class Square binh phuong bieu thuc
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018-10-09
 */

import static java.lang.Math.pow;

public class Square extends Expression {
    private Expression expression;

    /**
     * constructor
     * @param e bieu thuc
     */
    public Square(Expression e) { expression = e; }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    /**
     * Ham in bieu thuc
     * @return String
     */
    public String toString() {

        return String.format("(%s)^2 ", expression);
    }
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return (int) pow(expression.evaluate(), 2);
    }
}
