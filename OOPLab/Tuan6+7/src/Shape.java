/**
 * shape the hien thoc tinh cua hinh hoc
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */

public class Shape {
    // khai bao thuoc tinh cua class
    private String type;
    private String color;
    private double x, y;

    // ham khoi tao khong tham so
    public Shape(){
        this.type = "triangle";
        this.color = "red";
        this.x = 0;
        this.y = 0;
    }
    // ham khoi tao co tham so
    public Shape(String type, String color, double x, double y){
        this.type = type;
        this.color = color;
        this.x = x;
        this.y = y;
    }
    // ham setter
    public void setType(String type) {
        this.type = type;
    }
    // ham getter
    public String getType() {
        return type;
    }
    // ham setter
    public void setColor(String color) {
        this.color = color;
    }
    // ham getter
    public String getColor() {
        return color;
    }
    // ham setter
    public void setX(double x) {
        this.x = x;
    }
    // ham getter
    public double getX() {
        return x;
    }
    // ham setter
    public void setY(double y) {
        this.y = y;
    }
    // ham getter
    public double getY() {
        return y;
    }


    /**
     * moveXY dung de dich chuyen toa do hinh hoc
     * @param changeX toa do x thay doi
     * @param changeY toa do y thay doi
     */
    public void moveXY(double changeX, double changeY){
        this.x += changeX;
        this.y += changeY;
    }


    /**
     * ham show in thuoc tinh cua shape ra man hinh
     */
    public void show(){
        System.out.println(this.type);
        System.out.println(this.color);
        System.out.println(this.x);
        System.out.println(this.y);
    }
}
