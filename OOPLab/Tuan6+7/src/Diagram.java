/**
 * Diagram la 1 class quan ly class layer
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */
import java.sql.SQLOutput;
import java.util.ArrayList;

public class Diagram {
    //khai bao bien cua class
    private ArrayList<Layer> layerslist = new ArrayList<>();
    // khoi tao layer chua shape la circle
    public Layer layerCircle = new Layer();
    // khoi tao layer chua shape la rectangle
    public Layer layerRectangle = new Layer();
    // khoi tao layer chua shape la square
    public Layer layerSquare = new Layer();
    // khoi tao layer chua shape la triangle
    public Layer layerTriangle = new Layer();
    // khoi tao layer chua shape co hinh dang khac
    public Layer layerDifferntShape = new Layer();

    /**
     * ham deleteCircle dung de xoa het cas hinh la Circle
     */
    public void deleteCircle() {
        for (int i = 0; i < layerslist.size(); i++) {
            layerslist.get(i).deleteCircle1();
        }
    }


    /**
     * ham add them 1 layer vao layerlist
     * @param layer dai dien cho class Layer
     */
    public void add(Layer layer){
        layerslist.add(layer);
    }

    /**
     * ham transformShape chuyen tung loai hinh ve  vao 1 layer
     */
    public void transformShape(){
        for(int i = 0; i < layerslist.size(); i++) {
            for (Shape shape:layerslist.get(i).getShapeList()) {
                if(shape instanceof Circle){
                    layerCircle.add(shape);
                }
                else if (shape instanceof Triangle){
                    layerTriangle.add(shape);
                }
                else if(shape instanceof Square){
                    layerSquare.add(shape);
                }
                else  if(shape instanceof Rectangle){
                    layerRectangle.add(shape);
                }
                else layerDifferntShape.add(shape);
            }
        }
    }

    // ham getter
    public ArrayList<Layer> getLayerslist() {
        return layerslist;
    }

    /**
     * ham show in ket qua cua layerlist ra man hinh
     */
    public void show(){
        for(int i = 0; i < layerslist.size(); i++ ){
            layerslist.get(i).show();
        }
        System.out.println("@hinh tron:");
        for(int i = 0; i < layerCircle.getShapeList().size(); i++){
            layerCircle.getShapeList().get(i).show();
        }
        System.out.println("@hinh chu nhat:");
        for(int i = 0; i < layerRectangle.getShapeList().size(); i++){
            layerRectangle.getShapeList().get(i).show();
        }
        System.out.println("@hinh vuong:");
        for(int i = 0; i < layerSquare.getShapeList().size(); i++){
            layerSquare.getShapeList().get(i).show();
        }
        System.out.println("@hinh tam giac: ");
        for(int i = 0; i < layerTriangle.getShapeList().size(); i++){
            layerTriangle.getShapeList().get(i).show();
        }
        System.out.println("@hinh khac:");
        for(int i = 0; i < layerDifferntShape.getShapeList().size(); i++){

            layerDifferntShape.getShapeList().get(i).show();
        }
    }
}
