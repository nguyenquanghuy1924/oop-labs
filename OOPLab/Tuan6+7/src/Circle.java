/**
 * Circle bieu dien hinh tron
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */
public class Circle extends Shape{
    // khai bao thuoc tinh class
    private  double r;
    private  final  double pi = 3.14;

    // khoi tao khong tham so
    public Circle() {
        this.setType("circle");
        this.setColor("red");
        this.setX(0);
        this.setY(0);
        this.r = 1.0;
    }



    // khoi tao khong tham so
    public Circle(String color, double x, double y, double r){
        super("circle", color, x, y);
        this.r = r;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Circle other = (Circle) obj;
        return (this.getX() == other.getY() && this.getY() == other.getY() && this.getR() == other.getR());
    }

    // ham setter
    public void setR(double r) {
        this.r = r;
    }
    // ham getter
    public double getR() {
        return r;
    }
    public void show(){
        super.show();
        System.out.println(getR());
    }
}
