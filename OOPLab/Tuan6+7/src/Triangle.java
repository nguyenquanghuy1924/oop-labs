/**
 * Triangle
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */
public class Triangle extends Shape {
    // khai bao thuoc tinh class
    private  double a, b, c;

    // ham khoi tao khong tham so
    public Triangle(){
        this.setType("triangle");
        this.setColor("black");
        this.setX(0);
        this.setY(0);
        this.a = 3.0;
        this.b = 4.0;
        this.c = 5.0;
    }

    // ham khoi tao co tham so
    public Triangle(String color, double x, double y, double a, double b, double c){
        super("triange",color,x, y);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Triangle other = (Triangle) obj;
        return (this.getA() == other.getA() && this.getB() == other.getB() && this.getC() == other.getC() && this.getX()
        == other.getX() && this.getY() == other.getY());
    }

    //ham setter
    public void setA(double a) {
        this.a = a;
    }

    // ham setter
    public void setB(double b) {
        this.b = b;
    }
    //ham setter
    public void setC(double c) {
        this.c = c;
    }
    //ham getter
    public double getB() {
        return b;
    }
    // ham getter
    public double getA() {
        return a;
    }
    // ham getter
    public double getC() {
        return c;
    }

    public void show(){
        super.show();
        System.out.println(getA());
        System.out.println(getB());
        System.out.println(getC());
    }
}
