/**
 * Rectangle bieu dien hinh chu nhat
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */

public class Rectangle extends Shape{

    // thuoc tinh cua class
    private double weight;
    private double height;
    //ham khoi tao co khong tham so
    public Rectangle(){
        this.setType("rectangle");
        this.setColor("white");
        this.setX(0);
        this.setY(0);
        this.weight = 1.0;
        this.height = 1.0;
    }


    // ham khoi tao co tham so
    public Rectangle(String color, double x, double y, double weight, double height){
        super("retangcle", color, x, y );
        this.weight = weight;
        this.height = height;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Rectangle other = (Rectangle) obj;
        return (this.getHeight() == other.getHeight() && this.getWeight() == other.getWeight()
        && this.getX() == other.getX() && this.getY() == other.getY());
    }

    // ham setter
    public void setWeight(double weight) {
        this.weight = weight;
    }
    // ham getter
    public double getWeight() {
        return weight;
    }
    // ham setter
    public void setHeight(double height) {
        this.height = height;
    }
    // ham getter
    public double getHeight() {
        return height;
    }
    public void show(){
        super.show();
        System.out.println(getHeight());
        System.out.println(getWeight());
    }
}
