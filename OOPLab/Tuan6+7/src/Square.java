/**
 * square bieu dien hinh vuong
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */


public class Square extends Rectangle {
    // khai bao thuoc tinh cua class
    private double size;

    // ham khoi tao khong tham so
    public Square(){
        this.setType("square");
        this.setColor("blue");
        this.setX(0);
        this.setY(0);
        this.setHeight(1.0);
        this.setWeight(1.0);
        this.size = 0;
    }
    // ham khoi tao co tham so
    public Square(String color, double x, double y, double w, double h){
        super(color, x, y, w, h);
        this.setType("square");
        this.size = 1.0;
    }

    //ham setter
    public void setSize(double size) {
        this.size = size;
    }
    // ham getter
    public double getSize() {
        return size;
    }
    public double getWeight(){
        return getSize();
    }

    public double getHeight() {
        return getSize();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Square other = (Square) obj;
        return (this.getSize() == other.getSize() && this.getX() == other.getX() && this.getY() == other.getY());
    }

    public void show(){
        super.show();
        System.out.println(getSize());
    }
}
