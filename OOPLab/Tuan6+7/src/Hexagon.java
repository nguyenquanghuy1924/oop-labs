/**
 * square bieu dien luc giac deu
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */


public class Hexagon extends Shape {
    // khoi tao thuoc tinh cua class
    private double d;

    // ham khoi tao khong tham so
    public  Hexagon() {
        this.setType("hexagon");
        this.setColor("red");
        this.setX(0);
        this.setY(0);
        this.d = 1.0;
    }
    // khoi tao khong tham so
    public Hexagon(String color, double x, double y, double d) {
        super("Hexagon", color, x, y);
        this.d = d;
    }
    //ham setter
    public void setD(double d) {
        this.d = d;
    }
    //ham getter
    public double getD() {
        return d;
    }
}
