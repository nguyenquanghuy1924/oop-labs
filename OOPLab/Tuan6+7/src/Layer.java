/**
 * Layer la 1 class quan ly class shape
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */

import java.util.ArrayList;


public class Layer {
    // khai bao bien cua class
    private ArrayList<Shape> shapeList = new ArrayList<>();
    boolean visible = true;



    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }
    /**
     * ham add them 1 doi tuong shape vao shapelist
     * @param shape doi tuong shape
     */
    public void add(Shape shape){
        shapeList.add(shape);
    }

    /**
     * ham deleteTriangle dung de xoa hinh co ten Triangle
     */
    public void deleteTriangle(){
        for(int i = shapeList.size()-1; i >=0; i--){
                    if(shapeList.get(i).getType().equals("triangle")){
                        shapeList.remove(i);
            }
    }
    }


    /**
     * ham deleteCircle dung de xoa hinh co ten Circle
     */
    public void deleteCircle1(){
        for(int i = shapeList.size()-1; i >= 0; i --){
            if(shapeList.get(i).getType().equals("circle")){
                shapeList.remove(i);
            }
        }
    }

    public void deleteTheSame() {
        for (int i = shapeList.size() - 1; i >= 0; i--) {
            for (int j = 0; j < shapeList.size() ;  j++) {
                if (shapeList.get(i).equals(shapeList.get(j)) == true && i != j) {
                    shapeList.remove(i);
                    break;
                }
            }
        }
    }


    public ArrayList<Shape> getShapeList() {
        return shapeList;
    }

    /**
     * ham show dung de in ket cua shapelist ra man hinh
     */
    public void show(){
        for(int i = 0; i < shapeList.size(); i++){
            shapeList.get(i).show();
        }


    }

}
