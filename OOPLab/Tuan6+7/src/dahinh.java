/**
 * dahinh la 1 ung dung the hien
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 2018/10/9
 */

public class dahinh {

    /**
     * ham main test chuong trinh
     * @param args d, l, c lan luot la cac bien thuoc class diagram, layer, circle
     */
    public static void main(String[] args) {
        Diagram d = new Diagram();
        Layer l = new Layer();
        Layer l1 = new Layer();
        Shape c = new Circle();
        Shape s1 = new Hexagon();
        Shape s2 = new Circle("blue", 0.0, 0.0, 1.0);
        Shape s = new Triangle();
        l.add(c);
        //l.add(s);
        l.add(s2);
        l.add(s);
        d.add(l);
        l.deleteTheSame();
        d.transformShape();
        d.show();

    }
}
