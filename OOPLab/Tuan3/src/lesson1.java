/**
 * @ Lesson1 la ung dung de tim uoc chung lon nhat cua 2 so va tim day so fibonacci
 * @author Nguyen Quang Huy
 * @version 1.8
 * @2018-09-18
 */

public class lesson1 {
    /**
     * ham UCLN la ham tim uoc chung lon nhat cua 2 so nguyen
     * @param a so nguyen thu nhat
     * @param b so nguyen thu 2
     * @return uoc chung lon nhat cua 2 so
     */
    public  static  int UCLN(int a, int b){
        a = Math.abs(a);
        b = Math.abs(b);
        while(a != b){
            if(a > b){
                a = a - b;
            }
            if(a < b){
                b = a - b;
            }
        }
        return a;
    }

    /**
     * ham fib la ham tim so fibonacci thu n
     * @param n la vi tri so fibonacci can tim
     * @return so fibonacci thu n
     */
    public static  int fib(int n){
        if( n == 0 || n == 1){
            return 1;
        }
        else{
            return fib(n-1) + fib(n-2);
        }
    }

    /**
     * ham main de chay chuong trinh tim uoc chung lon nhat va day so fibonacci
     * @param args khong su dung
     */
    public static void main(String[] args) {
        System.out.println(UCLN(10, 2));
        for(int i = 0; i < 5; i ++){
            System.out.print(fib(i) +  " ");
        }
    }
}
