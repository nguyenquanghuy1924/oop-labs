/**
 * @ Lesson2 la ung dung de bieu dien phan so
 * @author Nguyen Quang Huy
 * @version 1.8
 * @2018-09-18
 */

class PS{
    // khai bao thuoc tinh class PS
    private int tuso;
    private int mauso;

    /**
     * PS la ham khoi tao cua class Ps
     * @param tu gia tri khoi tao cua tu so
     * @param mau gia tri khoi tao cua mau so
     */
    public  PS(int tu, int mau){
        this.tuso = tu;
        this.mauso = mau;
    }

    /**
     * UCLN la ham de tim uoc chung lon nhat cua 2 so nguyen
     * @param a so nguyen thu nhat
     * @param b so nguyen thu hai
     * @return uoc chung lon nhat cua 2 so
     */
    public int UCLN(int a, int b){
        a = Math.abs(a);
        b = Math.abs(b);
        while(a != b){
            if( a > b){
                a = a-b;
            }
            if (b > a){
                b = b-a;
            }
        }
        return a;
    }

    /**
     * PS cong la ham de cong 2 phan so
     * @param b bieu dien phan so b
     * @return ket qua cua phep tinh cong 2 phan so
     */
    public PS cong(PS b){
        PS result = new PS(0, 0);
        result.tuso = this.tuso*b.mauso + this.mauso*b.tuso;
        result.mauso = this.mauso*b.mauso;
        int t = UCLN(result.tuso, result.mauso);
        result.tuso = result.tuso/t;
        result.mauso = result.mauso/t;
        return result;
    }

    /**
     * PS tru la ham de tru 2 phan so
     * @param b bieu dien phan so b
     * @return ket qua cua phep tinh tru 2  phan so
     */
    public PS tru(PS b){
        PS result = new PS(0, 0);
        result.tuso = this.tuso*b.mauso - this.mauso*b.tuso;
        result.mauso = this.mauso*b.mauso;
        int t = UCLN(result.tuso, result.mauso);
        result.tuso = result.tuso/t;
        result.mauso = result.mauso/t;
        return result;
    }

    /**
     * PS nhan la ham de nhan 2 phan so
     * @param b bieu dien phan so b
     * @return ket qua cua phep tinh nhan 2 phan so
     */
    public PS nhan(PS b){
        PS result = new PS(0, 0);
        result.tuso = this.tuso*b.tuso;
        result.mauso = this.mauso*b.mauso;
        int t = UCLN(result.tuso, result.mauso);
        result.tuso = result.tuso/t;
        result.mauso = result.mauso/t;
        return result;
    }

    /**
     * PS chia la ham de chia 2 phan so
     * @param b bieu dien phan so b
     * @return ket qua cua phep tinh chia 2 phan so
     */
    public PS chia(PS b){
        PS result = new PS(0, 0);
        result.tuso = this.tuso*b.mauso;
        result.mauso = this.mauso*b.tuso;
        int t = UCLN(result.tuso, result.mauso);
        result.tuso = result.tuso/t;
        result.mauso = result.mauso/t;
        return result;
    }

    /**
     * ham equals kiem tra 2 phan so bang nhau
     * @param b bieu dien phan so b
     * @return True neu 2 phan so bang nhau va nguoc lai
     */
    public boolean equals(PS b){
        int t = UCLN(this.tuso, this.mauso);
        int h = UCLN(b.tuso, b.mauso);
        int a = this.tuso/t; int e = this.mauso/t; int c = b.tuso/h; int d = b.mauso/h;
        return (a*d == e*c);
    }

    //setter
    public int getTuso() {
        return this.tuso;
    }
    //getter
    public int getMauso(){
        return this.mauso;
    }

}

public class lesson2 {
    /**
     * ham main thuc hien bieu dien phan so va cac phep tinh toan phan so
     * @param args
     */

    public static void main(String[] args) {
        PS a = new PS(1,2);
        PS b = new PS(1,4);
        PS t = new PS(0,0);
        t = a.cong(b);
        System.out.print(t.getTuso() + "/");
        System.out.print(t.getMauso());
    }
}

