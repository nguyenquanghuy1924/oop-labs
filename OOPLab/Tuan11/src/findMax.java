/**
 * findMax tim ra max cua day su dung generics
 * @author Nguyen Quang Huy
 * @version 1.0
 * @since 26/11/2018
 */


import java.util.ArrayList;

public class findMax {
    /**
     * tim max cua day
     * @param a day a
     * @param <T> bien generics
     * @return max
     */
    public static <T extends Comparable> T max(ArrayList<T> a){
        T maxx = a.get(0);
        for(int i = 0; i < a.size(); i ++){
            if(a.get(i).compareTo(maxx) > 0){
                maxx = a.get(i);
            }
        }
        return maxx;
    }

        public static void main(String[] args)
        {
            ArrayList<Integer> arr = new ArrayList<Integer>();
            arr.add(0); arr.add(9); arr.add(2); arr.add(7); arr.add(10);
            System.out.println(max(arr));
            ArrayList<Double> arr1 = new ArrayList<Double>();
            arr1.add(0.5); arr1.add(9.1); arr1.add(4.2); arr1.add(7.2); arr1.add(9.12);
            System.out.println(max(arr1));
        }


}
