package OOP10;

import java.util.Random;
public class bai2 {
        static void bubbleSort(double[] arr) {
            int n = arr.length;
            double temp = 0;
            for(int i=0; i < n; i++){
                for(int j=1; j < (n-i); j++){
                    if(arr[j-1] > arr[j]){
                        //swap elements
                        temp = arr[j-1];
                        arr[j-1] = arr[j];
                        arr[j] = temp;
                    }

                }
            }

        }
        public static void main(String[] args) {


            Random rand = new Random();


            double arr[] = new  double[1000];
            for (int i = 0; i < 1000; i ++){
                double n = rand.nextDouble() + rand.nextInt(1000);

                arr[i] = n;
            }

            System.out.println("Array Before Bubble Sort");
            for(int i=0; i < arr.length; i++){
                System.out.print(arr[i] + " ");
            }
            System.out.println();

            bubbleSort(arr);//sorting array elements using bubble sort

            System.out.println("Array After Bubble Sort");
            for(int i=0; i < arr.length; i++){
                System.out.print(arr[i] + " ");
            }

        }
    }
